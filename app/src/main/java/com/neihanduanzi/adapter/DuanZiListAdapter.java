package com.neihanduanzi.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.neihanduanzi.R;
import com.neihanduanzi.model.DuanZi;
import com.neihanduanzi.myview.CircleImageView;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;


/**
 * Created by Administrator on 2016/10/31 0031.
 */

public class DuanZiListAdapter extends BaseAdapter {

    private Context mContext;
    private List<DuanZi.Data> mList;
//    private boolean flags = true;

    public DuanZiListAdapter(Context mContext, List<DuanZi.Data> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        int ret = 0;
        if (mList != null) {
            ret = mList.size();
        }
        return ret;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View ret = null;
        if (convertView != null) {
            ret = convertView;
        } else {
            ret = LayoutInflater.from(mContext).inflate(R.layout.item_duanzi, parent, false);
        }

        ViewHolder holder = (ViewHolder) ret.getTag();
        if (holder == null) {
            holder = new ViewHolder(ret);
            ret.setTag(holder);
        }
        holder.bindView(position, mList.get(position));
        holder.Share.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                EventBus.getDefault().post(mList.get(position));
            }
        });

        return ret;
    }

    private class ViewHolder implements View.OnClickListener {

        private final CircleImageView user_image;
        private final TextView userName;
        private final TextView content;
        private final TextView shareFrom;
        private final CheckBox Digg;
        private final CheckBox Bury;
        private final CheckBox Discuss;
        private final CheckBox Share;
        private final TextView diggOne;
        private final TextView buryOne;
        private final Animation animation;
        private int digg_count;
        private int bury_count;

        private DuanZi mData;

        public ViewHolder(View v) {
            user_image = (CircleImageView) v.findViewById(R.id.user_image);
            userName = (TextView) v.findViewById(R.id.user_name);
            content = (TextView) v.findViewById(R.id.content_neihan);
            shareFrom = (TextView) v.findViewById(R.id.share_from);
            Digg = (CheckBox) v.findViewById(R.id.duanzi_zan);
            Bury = (CheckBox) v.findViewById(R.id.duanzi_cai);
            Discuss = (CheckBox) v.findViewById(R.id.duanzi_discuss_count);
            Share = (CheckBox) v.findViewById(R.id.shar_view);

            diggOne = (TextView) v.findViewById(R.id.digg_one);
            buryOne = (TextView) v.findViewById(R.id.bury_one);

            animation = AnimationUtils.loadAnimation(mContext, R.anim.nn);

        }

        public void bindView(int position, final DuanZi.Data duanZi) {
            if (duanZi.getType() == 1) {

                if (user_image != null) {
                    String cover = duanZi.getGroup().getUser().getAvatar_url();

                    if (cover != null) {
                        Context context = user_image.getContext();
                        Picasso.with(context)
                                .load(cover)
                                .resize(320, 200)
                                .config(Bitmap.Config.ARGB_8888)
                                .into(user_image);
                    }
                }
                SharedPreferences sharedPreferences = mContext.getSharedPreferences("FontSetting", Context.MODE_PRIVATE);
                float font1 = sharedPreferences.getFloat("Font1", 18);
                float font2 = sharedPreferences.getFloat("Font2", 20);
                content.setTextSize(font1);

                userName.setText(duanZi.getGroup().getUser().getName());
                content.setText(duanZi.getGroup().getContent());
                shareFrom.setText(duanZi.getGroup().getCategory_name());
                digg_count = duanZi.getGroup().getDigg_count();
                Digg.setText(digg_count + "");
                bury_count = duanZi.getGroup().getBury_count();
                Bury.setText(bury_count + "");
                Discuss.setText(duanZi.getGroup().getComment_count() + "");
                Share.setText(duanZi.getGroup().getShare_count() + "");

                Share.setOnClickListener(this);
                Digg.setOnClickListener(this);
                Bury.setOnClickListener(this);


            }
       
    }

        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id) {
                case R.id.duanzi_zan:
                    if (true) {
                        diggOne.setVisibility(View.VISIBLE);
                        diggOne.startAnimation(animation);
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                diggOne.setVisibility(View.GONE);
                            }
                        }, 1000);
                        digg_count = digg_count + 1;
                        Digg.setText(digg_count + "");
                    }
                    break;
                case R.id.duanzi_cai:
                    buryOne.setVisibility(View.VISIBLE);
                    buryOne.startAnimation(animation);
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            buryOne.setVisibility(View.GONE);
                        }
                    }, 1000);
                    bury_count = bury_count + 1;
                    Bury.setText(bury_count + "");
                    break;

            }
        }


    }


}
