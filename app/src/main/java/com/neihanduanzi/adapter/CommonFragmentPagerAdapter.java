package com.neihanduanzi.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.neihanduanzi.fragment.BaseFragment;

import java.util.List;

public class CommonFragmentPagerAdapter extends FragmentPagerAdapter {

    private List<BaseFragment> mFragments;
    public CommonFragmentPagerAdapter(FragmentManager fm, List<BaseFragment> fragments) {
        super(fm);
        mFragments = fragments;
    }

    /**
     * 返回指定页码的 Fragment，用于显示 title
     */
    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    /**
     * 返回ViewPager的Fragment个数
     */
    @Override
    public int getCount() {
        int ret = 0;
        if(mFragments != null){
            ret = mFragments.size();
        }
        return ret;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragments.get(position).getFragmentTitle();
    }
}
