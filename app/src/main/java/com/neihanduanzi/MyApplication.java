package com.neihanduanzi;

import android.app.Application;
import android.graphics.Bitmap;

import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;

import org.xutils.*;

/**
 * Created by 凸显 on 2016/11/3.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        x.Ext.init(this);  //  初始化xUtils框架
        x.Ext.setDebug(org.xutils.BuildConfig.DEBUG);
        initPicasso();
    }

    private void initPicasso() {
        int maxSize = (int) (Runtime.getRuntime().maxMemory() / 8);
        Picasso picasso = new Picasso.Builder(this)
                .memoryCache(new LruCache(maxSize))
                .indicatorsEnabled(false)
                .defaultBitmapConfig(Bitmap.Config.RGB_565)
                .build();
        Picasso.setSingletonInstance(picasso);
    }

}
