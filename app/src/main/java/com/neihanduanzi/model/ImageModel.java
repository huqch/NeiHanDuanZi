package com.neihanduanzi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2016/11/1 0001.
 */

public class ImageModel {
    @SerializedName("data")
    private List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }


    public static class Data{
        @SerializedName("type")
        private int type;
        @SerializedName("group")
        private Group group;

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public Group getGroup() {
            return group;
        }

        public void setGroup(Group group) {
            this.group = group;
        }

        public static class Group{
            @SerializedName("group_id")
            public long group_id;
            @SerializedName("content")
            private String content;
            @SerializedName("comment_count")
            private int comment_count;
            @SerializedName("category_name")
            private String category_name;
            @SerializedName("bury_count")
            private int bury_count;
            @SerializedName("digg_count")
            private int digg_count;
            @SerializedName("share_count")
            private int share_count;
            @SerializedName("user")
            private User user;
            @SerializedName("large_image")
            private LargeImage large_image;
            @SerializedName("middle_image")
            private MiddleImage middle_image;
            @SerializedName("is_gif")
            private int is_gif;

            public long getGroup_id() {
                return group_id;
            }

            public void setGroup_id(long group_id) {
                this.group_id = group_id;
            }

            public String getCategory_name() {
                return category_name;
            }

            public void setCategory_name(String category_name) {
                this.category_name = category_name;
            }

            public MiddleImage getMiddle_image() {
                return middle_image;
            }

            public void setMiddle_image(MiddleImage middle_image) {
                this.middle_image = middle_image;
            }

            public int getIs_gif() {
                return is_gif;
            }

            public void setIs_gif(int is_gif) {
                this.is_gif = is_gif;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public int getComment_count() {
                return comment_count;
            }

            public void setComment_count(int comment_count) {
                this.comment_count = comment_count;
            }

            public int getBury_count() {
                return bury_count;
            }

            public void setBury_count(int bury_count) {
                this.bury_count = bury_count;
            }

            public int getDigg_count() {
                return digg_count;
            }

            public void setDigg_count(int digg_count) {
                this.digg_count = digg_count;
            }

            public int getShare_count() {
                return share_count;
            }

            public void setShare_count(int share_count) {
                this.share_count = share_count;
            }

            public User getUser() {
                return user;
            }

            public void setUser(User user) {
                this.user = user;
            }

            public LargeImage getLarge_image() {
                return large_image;
            }

            public void setLarge_image(LargeImage large_image) {
                this.large_image = large_image;
            }

            public static class User{
                @SerializedName("name")
                private String name;
                @SerializedName("avatar_url")
                private String avatar_url;

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getAvatar_url() {
                    return avatar_url;
                }

                public void setAvatar_url(String avatar_url) {
                    this.avatar_url = avatar_url;
                }


            }

            public static class LargeImage{
                @SerializedName("width")
                private int width;
                @SerializedName("r_height")
                private int r_height;
                @SerializedName("r_width")
                private int r_width;
                @SerializedName("uri")
                private String uri;
                @SerializedName("height")
                private int height;
                @SerializedName("url_list")
                private List<UrlList> url_list;

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public int getR_height() {
                    return r_height;
                }

                public void setR_height(int r_height) {
                    this.r_height = r_height;
                }

                public int getR_width() {
                    return r_width;
                }

                public void setR_width(int r_width) {
                    this.r_width = r_width;
                }

                public String getUri() {
                    return uri;
                }

                public void setUri(String uri) {
                    this.uri = uri;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }

                public List<UrlList> getUrl_list() {
                    return url_list;
                }
                public void setUrl_list(List<UrlList> url_list) {
                    this.url_list = url_list;
                }



                public static class UrlList {
                    @SerializedName("url")
                    private String url;
                    public String getUrl() {
                        return url;
                    }
                    public void setUrl(String url) {
                        this.url = url;
                    }
                }
            }

            public static class MiddleImage{
                @SerializedName("width")
                private int width;
                @SerializedName("r_height")
                private int r_height;
                @SerializedName("r_width")
                private int r_width;
                @SerializedName("uri")
                private String uri;
                @SerializedName("height")
                private int height;
                @SerializedName("url_list")
                private List<UrlList> url_list;

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public int getR_height() {
                    return r_height;
                }

                public void setR_height(int r_height) {
                    this.r_height = r_height;
                }

                public int getR_width() {
                    return r_width;
                }

                public void setR_width(int r_width) {
                    this.r_width = r_width;
                }

                public String getUri() {
                    return uri;
                }

                public void setUri(String uri) {
                    this.uri = uri;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }

                public List<UrlList> getUrl_list() {
                    return url_list;
                }
                public void setUrl_list(List<UrlList> url_list) {
                    this.url_list = url_list;
                }



                public static class UrlList {
                    @SerializedName("url")
                    private String url;
                    public String getUrl() {
                        return url;
                    }
                    public void setUrl(String url) {
                        this.url = url;
                    }
                }
            }

        }


    }
}
