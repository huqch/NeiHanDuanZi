package com.neihanduanzi.model;

import com.google.gson.annotations.SerializedName;

import org.xutils.db.annotation.Table;

/**
 * Created by 凸显 on 2016/10/31.
 */

/**
 * 用户实体类
 */
@Table(name = "user.db")
public class User {
    @SerializedName("user_id")
    private long id;
    @SerializedName("name")
    private String userName;
    @SerializedName("avatar_url")
    private String avatarUrl;
    @SerializedName("followings")
    private int followings;
    @SerializedName("followers")
    private int followers;
    @SerializedName("ugc_count")
    private int ugcCount;
    @SerializedName("is_following")
    private boolean isFollowing;
    @SerializedName("user_verified")
    private boolean userVerified;


    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public int getFollowings() {
        return followings;
    }

    public void setFollowings(int followings) {
        this.followings = followings;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isFollowing() {
        return isFollowing;
    }

    public void setFollowing(boolean following) {
        isFollowing = following;
    }

    public int getUgcCount() {
        return ugcCount;
    }

    public void setUgcCount(int ugcCount) {
        this.ugcCount = ugcCount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isUserVerified() {
        return userVerified;
    }

    public void setUserVerified(boolean userVerified) {
        this.userVerified = userVerified;
    }
}
