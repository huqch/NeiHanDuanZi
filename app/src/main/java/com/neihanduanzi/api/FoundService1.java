package com.neihanduanzi.api;

import com.neihanduanzi.model.FoundModel1;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Administrator on 2016/10/31 0031.
 */

public interface FoundService1 {
    @GET("2/essay/discovery/v3/?iid=2767929313&device_id=2757969807&ac=wifi&channel=baidu2&aid=7&app_name=joke_essay&version_code=400&device_platform=android&device_type=KFTT&os_api=15&os_version=4.0.3&openudid=b90ca6a3a19a78d6")
    Call<FoundModel1> getInfo();
}
